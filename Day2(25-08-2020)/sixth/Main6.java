package fresher;

import java.util.Scanner;

class ExtraType
{
	private String name;
	private long runs;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getRuns() {
		return runs;
	}
	public void setRuns(long runs) {
		this.runs = runs;
	}
	ExtraType(String name,long runs)
	{
		this.name=name;
		this.runs=runs;
	}
	void display()
	{
	System.out.println("ExtraType Details :");
	System.out.println("ExtraType : "+name);
	System.out.println("Runs :"+runs);
	}
	
}
public class Main6 {
public static void main(String[] args) {
	Scanner s=new Scanner(System.in);
	System.out.println("Enter the extratype Details");
	String details=s.nextLine();
	String arr[]=details.split("#");
	long run=Long.parseLong(arr[1]);
	ExtraType e=new ExtraType(arr[0],run);
	e.display();
	
	
	
}
}
