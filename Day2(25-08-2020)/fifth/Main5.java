package fresher;

import java.util.Scanner;

class Venue2
{
	
	private String name;
	private String city;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	Venue2(String name,String city)
	{
		this.name=name;
		this.city=city;
	}
		void display()
		{
			System.out.println("Venue Details: ");
			System.out.println("Venue Name : "+name);
			System.out.println("City Name: "+city);
			
		}
	
}
public class Main5 {
public static void main(String[] args) {
	int option=0;
	Scanner s=new Scanner(System.in);
	System.out.println("Enter the Venue Name");
	String name=s.nextLine();
	System.out.println("Enter the City Name");
	String city=s.nextLine();
	Venue2 v=new Venue2(name,city);
	v.display();
	do
	{
	System.out.println("Verify and Update Venue Details\nMenu\n1.Update Venue Name\n2.Update City Name\n3.All informations Correct/Exit\nType 1 or 2 or 3");
	option=s.nextInt();
	s.nextLine();
	if(option==1) {
		System.out.println("Enter the new Venue Name");
		String newvenue=s.nextLine();
		v.setName(newvenue);
		v.display();

	}
	else if(option==2) {
		System.out.println("Enter the City Name");
		String c=s.nextLine();
		v.setCity(c);
		v.display();
	
	}
	else if(option==3)
	{
		v.display();
	}
	}while(option!=3);
}

}

