package fresher2;

public class Wicket {
private Long over;
private long ball;
private String wicketType;
public Wicket() {
	super();
	// TODO Auto-generated constructor stub
}
private String playerName;
public Wicket(Long over, long ball, String wicketType, String playerName, String bowlerName) {
	super();
	this.over = over;
	this.ball = ball;
	this.wicketType = wicketType;
	this.playerName = playerName;
	this.bowlerName = bowlerName;
}
private String bowlerName;
public Long getOver() {
	return over;
}
public void setOver(Long over) {
	this.over = over;
}
public long getBall() {
	return ball;
}
public void setBall(long ball) {
	this.ball = ball;
}
public String getWicketType() {
	return wicketType;
}
public void setWicketType(String wicketType) {
	this.wicketType = wicketType;
}
public String getPlayerName() {
	return playerName;
}
public void setPlayerName(String playerName) {
	this.playerName = playerName;
}
public String getBowlerName() {
	return bowlerName;
}
public void setBowlerName(String bowlerName) {
	this.bowlerName = bowlerName;
}
void display()
{
	System.out.println("Wicket Details");
	System.out.println("Over :"+over);
	System.out.println("Ball :"+ball);
	System.out.println("WicketType :"+wicketType);
	System.out.println("PlayerName :"+playerName);
	System.out.println("BowlerName :"+bowlerName);
}

}
