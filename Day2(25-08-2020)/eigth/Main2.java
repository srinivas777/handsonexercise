package fresher2;

import java.util.Scanner;

public class Main2 {
public static void main(String[] args) {
	Scanner s=new Scanner(System.in);
	System.out.println("Enter the number of Wickets");
	int w=s.nextInt();
	s.nextLine();
	Wicket wick=new Wicket();
	for(int i=1;i<=w;i++)
	{
	System.out.println("Enter the details of Wicket "+i);
	String details=s.nextLine();
	String detailsarray[]=details.split(",");
	long over=Long.parseLong(detailsarray[0]);
	long ball=Long.parseLong(detailsarray[1]);
	wick.setOver(over);
	wick.setBall(ball);
	wick.setWicketType(detailsarray[2]);
	wick.setPlayerName(detailsarray[3]);
	wick.setBowlerName(detailsarray[4]);
	wick.display();
	}
}
}