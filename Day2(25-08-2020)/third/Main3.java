package fresher;

import java.util.Scanner;

class Delivery
{
	public long over;
	public long ball;
	public long runs;
	public String batsman;
	public String bowler;
	public String nonstriker;
	Delivery(long over,long ball,long runs,String batsman,String bowler,String nonstriker)
	{
		this.over=over;
		this.ball=ball;
		this.runs=runs;
		this.batsman=batsman;
		this.bowler=bowler;
		this.nonstriker=nonstriker;
	}
	public void displayDeliveryDetails() {
		System.out.println("Delivery Details : ");
		System.out.println("Over : "+ over);
		System.out.println("Ball :"+ ball);
		System.out.println("Runs : "+ runs);
		System.out.println("Batsman : "+ batsman);
		System.out.println("Bowler : "+ bowler);
		System.out.println("Non Striker : " + nonstriker);

	}
}
public class Main3 {
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
			
			System.out.println("Enter the over");
			String o=s.nextLine();
			long over=Long.parseLong(o);
			System.out.println("Enter the ball");
			String b=s.nextLine();
			Long ball=Long.parseLong(b);
			System.out.println("Enter the runs");
			String r=s.nextLine();
			Long runs=Long.parseLong(r);
			System.out.println("Enter the batsman name");
			String bat=s.nextLine();
			System.out.println("Enter the bowler name");
			String bowl=s.nextLine();
			System.out.println("Enter the nonstriker name");
			String ns=s.nextLine();
			Delivery d=new Delivery(over,ball,runs,bat,bowl,ns);
			d.displayDeliveryDetails();
}
}
