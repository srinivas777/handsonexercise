package day27;

import java.util.Scanner;

public class Exception1 {
public static void main(String[] args) {
	int num_of_overs=0;
	try {
	Scanner s=new Scanner(System.in);
	System.out.println("Enter the number of overs");
	num_of_overs=s.nextInt();
	System.out.println("Enter the number of runs for each over");
	int number_of_runs[]=new int[num_of_overs];
	for(int i=0;i<number_of_runs.length;i++)
	{
		number_of_runs[i]=s.nextInt();
	}
	System.out.println("Enter the over number");
	int overnumber=s.nextInt();
	System.out.println("Runs scored in this over: "+number_of_runs[overnumber-1]);
	}
	catch(NegativeArraySizeException e)
	{
		System.out.println(e);
	}
	catch(ArrayIndexOutOfBoundsException e)
	{
		System.out.println("java.lang.ArrayIndexOutOfBoundsException");
	}
	
}
}
