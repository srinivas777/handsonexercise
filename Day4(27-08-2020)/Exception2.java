package day27;

import java.util.Scanner;
class CustomException extends Exception
{
	public CustomException(String message)
	{
	super(message);
	}
}
public class Exception2{
	public static void main(String[] args) {
	Scanner s=new Scanner(System.in);
	System.out.println("Enter the Player name ");
	String name=s.nextLine();
	System.out.println("Enter the player age");
	try
	{
	int age=s.nextInt();
	if(age<19)
	{
		throw new CustomException("CustomException: InvalidAgeRangeException");
	}
	else
	{
		System.out.println("Player name :"+name);
		System.out.println("Player age :"+age);
	}
	}
	catch(CustomException e)
	{
		System.out.println(e.getMessage());
		
	}

}
}
