package sept8;

import java.util.*;
class Player
{
	private String name;
	private String team;
	private String skill;
	public Player(String name, String team, String skill) {
		super();
		this.name = name;
		this.team = team;
		this.skill = skill;
	}
	@Override
	public String toString() {
		return name+"--"+team+"--"+skill;
	}
	
}
public class Fourth {
public static void main(String[] args) {
	Scanner s=new Scanner(System.in);
	System.out.println("Enter number of Players");
	int num,capnum;String name,team,skill;
	num=s.nextInt();
	s.nextLine();
	List<Player>l=new ArrayList<Player>();
	TreeMap<Integer,Player> t=new TreeMap<>();
	for(int i=0;i<num;i++)
	{
		System.out.println("Enter the details of Player"+(i+1));
		capnum=s.nextInt();
		s.nextLine();
		name=s.nextLine();
		team=s.nextLine();
		skill=s.nextLine();
		t.put(capnum,new Player(name,team,skill));
		
	}
	 Set st=t.entrySet();
     Iterator i=st.iterator();
     System.out.println("Player Details");
     while(i.hasNext()){
                  Map.Entry m=(Map.Entry)  i.next();
                  System.out.println(m.getKey()+"--"+m.getValue());
     }
}

}
