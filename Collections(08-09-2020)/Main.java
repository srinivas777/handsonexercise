package sept8;
import java.util.*;
class Team {
private String name;
private long numberOfMatches;
public Team(String name, long numberOfMatches) {
	super();
	this.name = name;
	this.numberOfMatches = numberOfMatches;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public long getNumberOfMatches() {
	return numberOfMatches;
}
@Override
public String toString() {
	return name+"-"+numberOfMatches;
}
public void setNumberOfMatches(long numberOfMatches) {
	this.numberOfMatches = numberOfMatches;
}

}
public class Main
{
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("Enter number of teams:");
		int num;String name;int numOfMatches;
		num=s.nextInt();
		s.nextLine();
		List<Team>l=new ArrayList<Team>();
		for(int i=0;i<num;i++)
		{
			System.out.println("Enter team "+(i+1)+" detail");
			System.out.println("Enter Name");
			name=s.nextLine();
			System.out.println("Enter number of matches");
			numOfMatches=s.nextInt();
			s.nextLine();
			l.add(new Team(name,numOfMatches));
		}
		Collections.sort(l,new TeamComparator());
		System.out.println("Team list after sort by number of matches");
		for(int i=0;i<l.size();i++)
		{
			System.out.println(l.get(i));
		}
	}
}
