package day26;

import java.util.Scanner;

class Shape {
protected String shapeName;

public Shape(String shapeName) {
	this.shapeName = shapeName;
}
public double calculateArea()
{
	return 0.0; 
}
}
class Square extends Shape
{
	private int side;
	public Square(String shapeName, int side) {
		super(shapeName);
		this.side = side;
	}
	public double calculateArea()
	{
		return side*side;
	}
}
class Rectangle extends Shape
{
	private int length;
	private int bredth;
	public Rectangle(String shapeName, int length, int bredth) {
		super(shapeName);
		this.length = length;
		this.bredth = bredth;
	}
	public double calculateArea()
	{
		return length*bredth;
	}
	
}
class Circle extends Shape
{
	private int radius;

	public Circle(String shapeName, int radius) {
		super(shapeName);
		this.radius = radius;
	}
	public double calculateArea()
	{
		return 3.14*radius*radius;
	}
}
public class ShapeMain
{
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		System.out.println("1. Rectangle\n2. Square\n3. Circle\nArea Calculator --- Choose your Shape");
		int opt=s.nextInt();s.nextLine();
		if(opt==1)
		{
			System.out.println("Enter length and bredth");
			int l=s.nextInt();
			int b=s.nextInt();
			Rectangle r=new Rectangle("rectangle", l, b);
			System.out.println("Area of rectangle :"+r.calculateArea());
		}
		else if(opt==2)
		{
			System.out.println("Enter side");
			int sid=s.nextInt();
			Square square=new Square("square", sid);
			System.out.println("Area of square :"+square.calculateArea());
		}
		else if(opt==3)
		{
			System.out.println("Enter radius");
			int radius=s.nextInt();
			Circle c=new Circle("Circle", radius);
			System.out.println("Area of Circle :"+c.calculateArea());
			
		}
	}
}

