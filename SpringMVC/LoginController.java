package com.assign2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


import com.assign2.model.Login;


@Controller
public class LoginController {
@RequestMapping(value="/login")
	public String login(ModelMap m)
	{
	Login l=new Login();
	m.addAttribute("logintable", l);
	return "login";
	}
@RequestMapping(value="/check")
public String login(@Validated @ModelAttribute("logintable")Login l,BindingResult result,ModelMap m1)
{
	String finalresult="";
	if(result.hasErrors())
	{
		finalresult="login";
	}
	else
	{
	m1.addAttribute("log",l);
	finalresult= "success";
	}
	return finalresult;
	}

}
